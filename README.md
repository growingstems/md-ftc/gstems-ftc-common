# growingSTEMS FTC Common Library
master: [![pipeline status](https://gitlab.com/growingstems/md-ftc/gstems-ftc-common/badges/master/pipeline.svg)](https://gitlab.com/growingstems/md-ftc/gstems-ftc-common/-/commits/master)<br>
dev: [![pipeline status](https://gitlab.com/growingstems/md-ftc/gstems-ftc-common/badges/dev/pipeline.svg)](https://gitlab.com/growingstems/md-ftc/gstems-ftc-common/-/commits/dev)<br>
[JavaDoc Here!](https://growingstems.gitlab.io/md-ftc/gstems-ftc-common/)<br><br>
Java library with general purpose library funtionality as well as FRC specific command based library functionality.<br>
Requires EditorConfig plugin in VS Code to use .editorconfig file<br>
For usage, see [this wiki page on GitLab](https://gitlab.com/growingstems/md-ftc/gstems-ftc-common/-/wikis/Normal%20Usage)

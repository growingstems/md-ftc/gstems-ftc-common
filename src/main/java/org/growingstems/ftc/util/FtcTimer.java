/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.ftc.util;

import org.growingstems.util.timer.TimerBase;

/**
 * {@link org.growingstems.util.timer.TimerBase} implementation using
 * {@link edu.wpi.first.wpilibj.RobotController#getFPGATime()}. Precision: 1
 * microsecond
 */
public class FtcTimer extends TimerBase {
    /**
     * Gets the time using
     * {@link edu.wpi.first.wpilibj.RobotController#getFPGATime()}
     *
     * @return FPGA Time in seconds.
     */
    @Override
    protected double getTime() {
        return 0.0;
    }
}
